import { Component, OnInit } from '@angular/core';
// import { SpinnerService } from '../../shared/services/spinner.service';
@Component({
  selector: 'app-arrived-component',
  templateUrl: './arrived.component.html',
  styleUrls: ['./arrived.component.css']
})

export class ArrivedComponent implements OnInit {
  public awaitingPackages: any[] = [
    {
      trackingNumber: '1312312412312522313',
      additionalSecutiry: false,
      reBoundle: true,
      moveToPackage: true,
      isDeclared: true,
      comment: 'this is comment string here with some long text inside',
      arrivalEstimationDate: new Date(),
      arrivalDate: new Date(),
      sentDate: new Date(),
      status: {
        name: 'arrived'
      },
      sender: {
        senderType: 'personal',
        onlineStoreWebAddress: 'www.amazon.com'
      },
      packageItems: [
        {
          category: '9603',
          price: 20,
          currency: 'USD'
        },
        {
          category: '7013',
          price: 200,
          currency: 'USD'
        },
        {
          category: '9603',
          price: 20,
          currency: 'USD'
        },
        {
          category: '7013',
          price: 200,
          currency: 'USD'
        },
        {
          category: '9603',
          price: 20,
          currency: 'USD'
        },
        {
          category: '7013',
          price: 200,
          currency: 'USD'
        },
        {
          category: '9603',
          price: 20,
          currency: 'USD'
        },
        {
          category: '7013',
          price: 200,
          currency: 'USD'
        }
      ]
    },
    {
      trackingNumber: '82791823987987123',
      additionalSecutiry: true,
      reBoundle: true,
      moveToPackage: true,
      isDeclared: true,
      comment: 'this is comment string here with some long text inside',
      arrivalEstimationDate: new Date(),
      arrivalDate: new Date(),
      sentDate: new Date(),
      status: {
        name: 'arrived'
      },
      sender: {
        senderType: 'personal',
        onlineStoreWebAddress: 'www.amazon.com'
      },
      packageItems: [
        {
          category: '9603',
          price: 20,
          currency: 'USD'
        },
        {
          category: '9405',
          price: 200,
          currency: 'USD'
        }
      ]
    },
    {
      trackingNumber: '1312312412312522313',
      additionalSecutiry: true,
      reBoundle: true,
      moveToPackage: true,
      isDeclared: true,
      comment: 'this is comment string here with some long text inside',
      arrivalEstimationDate: new Date(),
      arrivalDate: new Date(),
      sentDate: new Date(),
      status: {
        name: 'arrived'
      },
      sender: {
        senderType: 'store',
        onlineStoreWebAddress: 'www.amazon.com'
      },
      packageItems: [
        {
          category: '9603',
          price: 20,
          currency: 'USD'
        },
        {
          category: '9405',
          price: 200,
          currency: 'USD'
        }
      ]
    },
    {
      trackingNumber: '1312312412312522313',
      additionalSecutiry: true,
      reBoundle: true,
      moveToPackage: true,
      isDeclared: true,
      comment: 'this is comment string here with some long text inside',
      arrivalEstimationDate: new Date(),
      arrivalDate: new Date(),
      sentDate: new Date(),
      status: {
        name: 'arrived'
      },
      sender: {
        senderType: 'personal',
        onlineStoreWebAddress: 'www.amazon.com'
      },
      packageItems: [
        {
          category: '71171999000',
          price: 20,
          currency: 'USD'
        },
        {
          category: '9603',
          price: 200,
          currency: 'USD'
        }
      ]
    },
    {
      trackingNumber: '82791823987987123',
      additionalSecutiry: true,
      reBoundle: true,
      moveToPackage: true,
      isDeclared: true,
      comment: 'this is comment string here with some long text inside',
      arrivalEstimationDate: new Date(),
      arrivalDate: new Date(),
      sentDate: new Date(),
      status: {
        name: 'arrived'
      },
      sender: {
        senderType: 'store',
        onlineStoreWebAddress: 'www.amazon.com'
      },
      packageItems: [
        {
          category: '9603',
          price: 20,
          currency: 'USD'
        },
        {
          category: '9603',
          price: 200,
          currency: 'USD'
        },
      ]
    },
    {
      trackingNumber: '1312312412312522313',
      additionalSecutiry: true,
      reBoundle: true,
      moveToPackage: true,
      isDeclared: true,
      comment: 'this is comment string here with some long text inside, this is comment string here with some long text inside, this is comment string here with some long text inside, this is comment string here with some long text inside, this is comment string here with some long text inside, this is comment string here with some long text inside, this is comment string here with some long text inside, this is comment string here with some long text inside',
      arrivalEstimationDate: new Date(),
      arrivalDate: new Date(),
      sentDate: new Date(),
      status: {
        name: 'arrived'
      },
      sender: {
        senderType: 'store',
        onlineStoreWebAddress: 'www.amazon.com'
      },
      packageItems: [
        {
          category: '9603',
          price: 20,
          currency: 'USD'
        },
        {
          category: '9603',
          price: 200,
          currency: 'USD'
        }
      ]
    }
  ];
  
  constructor() {

  }

  ngOnInit() {
    
  }
}
