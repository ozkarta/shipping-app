import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../shared/models/user.model';
import { ClientAddressService } from '../shared/services/address.service';
import { Address } from '../../shared/models/address.model';
import { SpinnerService } from '../../shared/services/spinner.service';
import { AuthService } from '../../shared/services/auth.service';
@Component({
	selector: 'app-client-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})

export class ClientHomeComponent implements OnInit {
	public user: UserModel = new UserModel();
	public passwordUpdateModelBinder: PasswordUpdateModel = <PasswordUpdateModel>{};
	public cities: string[] = [
    'თბილისი', 'ქუთაისი', 'ბათუმი', 
  ];
  public genders: Gender[] = [
    {
      value: 'MALE',
      viewValue: 'მამრობითი'
    },
    {
      value: 'FEMALE',
      viewValue: 'მდედრობითი'
    },
    {
      value: 'OTHER',
      viewValue: 'სხვა'
    }
  ];
	public userAccount: any = {
		receiptSum: 124,
		balance: 300,
		payable: 23,
	};
	public addresses: any[] = [];	

	constructor(
		private addressService: ClientAddressService,
		private spinnerService: SpinnerService,
		private authService: AuthService
	) {}

	ngOnInit() {
		this.authService.getUser.subscribe(
			(user: UserModel) => {
				this.user = user;
				this.initialLoad();
			}
		)
	}

	initialLoad() {
		this.spinnerService.next(true);
		this.addressService.getAllAddresses()
			.subscribe(
				(addresses: Address[]) => {
					this.addresses = addresses || [];
					this.spinnerService.next(false);
				},
				(error: Error) => {
					console.dir(error);
					this.spinnerService.next(false);
				}
			)
	}

	setPrefix(num: number, length: number): string {
		let result = '';
		let actualLength = length - `${num}`.length;
		for (let i=0; i<actualLength - 1; i++) {
			result += '0';
		}
		result += `${num}`;
		return result;
	}

	getUserName(user: UserModel) : string {
		let result = '';
		if (user && user.firstName) {
			result += user.firstName.en;
		}
		result += ' ';
		if (user && user.lastName) {
			result += user.lastName.en;
		}
		return result;
	}

	updatePassword() {

	}

	updateUserInfo() {
		this.spinnerService.next(true);
		this.authService.updateUserInfo(this.user)
			.subscribe(
				(success: any) => {
					this.spinnerService.next(false);
				},
				(error: Error) => {
					console.dir(error);
					this.spinnerService.next(false);
				}
			)
	}
	
}

interface Gender {
  value: string;
  viewValue: string;
}

interface PasswordUpdateModel {
	oldPassword: string;
	newPassword: string;
	newPasswordRe: string;
}