import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Module Imports
import { ClientRoutingModule } from './client-routing.module';
// Components
import { ClientHomeComponent } from './home/home.component';
import { ArrivedComponent } from './arrived/arrived.component';
import { DeliveredComponent } from './delivered/delivered.component';
import { InWareHouseComponent } from './in-warehouse/in-warehouse.component';
import { OnTheWareHouseWayComponent } from './on-the-warehouse-way/on-the-warehouse-way.component';
import { SentComponent } from './sent/sent.component';

import { PackageEditableListComponent } from './shared/components/package-editable-list/package-editable-list.component';
import { ViewableListComponent } from './shared/components/package-viewable-list/package-viewable-list.component';
// Dialogs
import { PackageDeclarationFormDialogComponent } from './shared/dialogs/package-declaration-form/package-declaration-form.component';
// Material
import { AngularMaterialModule } from '../shared/modules/material/mat-import.module';
// Services
import { ClientAddressService } from './shared/services/address.service';


@NgModule({
  declarations: [
    ClientHomeComponent,
    ArrivedComponent,
    DeliveredComponent,
    InWareHouseComponent,
    OnTheWareHouseWayComponent,
    SentComponent,
    PackageEditableListComponent,

    PackageDeclarationFormDialogComponent,
    ViewableListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
		HttpClientModule,
		
    ClientRoutingModule,
    
    // Material
    AngularMaterialModule
  ],
  providers: [
    ClientAddressService,
  ],
  entryComponents: [
    PackageDeclarationFormDialogComponent
  ]
})
export class ClientModule { }
