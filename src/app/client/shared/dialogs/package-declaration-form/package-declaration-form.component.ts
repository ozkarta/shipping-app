import { Component, Inject, ElementRef, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
@Component({
  selector: 'app-package-declaration-form-dialog-component',
  templateUrl: './package-declaration-form.component.html',
  styleUrls: ['./package-declaration-form.component.css']
})

export class PackageDeclarationFormDialogComponent {
  @ViewChild('scrollable') scrollable: ElementRef = <any>null;
  public senderTypes: any[] = [
    {value: 'personal', viewValue: 'პირადი გზავნილი'},
    {value: 'store', viewValue: 'ონლაინ მაღაზია'}
  ];

  public categories: any[] = [
    {value: '9603', viewValue: 'სხვადასხვა'},
    {value: '8708', viewValue: 'ავტო ნაწილები'},
    {value: '71171999000', viewValue: 'ბიჟუტერია'},
    {value: '9405', viewValue: 'განათება, ჭაღები, ლამფები, ფარები'},
    {value: '8205', viewValue: 'იარაღები და ხელის ინსტრუმენტები'},
    {value: '8471', viewValue: 'კომპიუტერი, ლეპტოპი და მათი ნაწილები'},
    {value: '3004', viewValue: 'მედიკამენტები'},
    {value: '7013', viewValue: 'მინის ნაწარმი'},
    {value: '9207', viewValue: 'მუსიკალური ინსტრუმენტები და მათი ნაწილები'},
    {value: '0602', viewValue: 'მცენარეები'},
    {value: '4901', viewValue: 'ნაბეჭდი პროდუქცია, წიგნები, ბროშურა'},
    {value: '8525', viewValue: 'ოპტიკური და ფოტო აპარატურა'},
    {value: '3304', viewValue: 'პარფიუმერია და კოსმეტიკა'},
    {value: '9102', viewValue: 'საათები'},
    {value: '9504', viewValue: 'სათამაშოები და სპორტული ინვენტარი'},
    {value: '2106', viewValue: 'საკვები დანამატები'},
    {value: '6204', viewValue: 'ტანსაცმელი, ყველა ტიპის სამოსი'},
    {value: '8517', viewValue: 'ტელეფონი და ქსელური მოწყობილობები'},
    {value: '6403', viewValue: 'ფეხსაცმელი'},
    {value: '4202', viewValue: 'ჩანთები და ჩასადებები'},
    {value: '8543', viewValue: 'სხვადასხვა ელექტრონული მოწყობილობები'},
  ];

  public currencies: any[] = [
    'USD', 'GEL', 'EUR'
  ]
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PackageDeclarationFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.dir(data);
  }

  addPackageItemClickHandler() {
    this.data.packageItems.push(
      {
        category: '9603',
          price: 0,
          currency: 'USD'
      }
    );
    setTimeout(() => this.scrollToBottom(this.scrollable), 200);
  }

  scrollToBottom(element: ElementRef, extraHeight?: number): void {
    try {
      element.nativeElement.scrollTop = element.nativeElement.scrollHeight + (extraHeight || 0);
    } catch (exception) {
      console.dir(exception);
    }
  }

  getItemsSum() {
    return this.data.packageItems.reduce((accumulator: number, item: any) => {
      accumulator+=item.price;
      return accumulator;
    }, 0);
  }

  onNoClick() {
    this.dialogRef.close({ cancel: true });
  }

  onYesClick() {
    this.dialogRef.close({ cancel: true });
  }
}