import { Component, OnInit, Input } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { PackageDeclarationFormDialogComponent } from '../../dialogs/package-declaration-form/package-declaration-form.component';

@Component({
  selector: 'app-package-editable-list-component',
  templateUrl: './package-editable-list.component.html',
  styleUrls: ['./package-editable-list.component.css']
})

export class PackageEditableListComponent implements OnInit {
  private openDialogReference: MatDialogRef<PackageDeclarationFormDialogComponent> = <any>undefined;
  @Input('awaitingPackages') awaitingPackages: any[] = [];
  @Input('showDelete') showDelete: boolean = false;
  @Input('title') title: string = '';
  constructor(public dialog: MatDialog) {

  }

  ngOnInit() {

  }


  openPackageDeclarationFormDialog(data: any): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        this.openDialogReference = this.dialog.open(PackageDeclarationFormDialogComponent, {
          width: '800px',
          data: data
        });

        this.openDialogReference.afterClosed().subscribe(result => {
          if (!result) {
            return reject();
          }
          if (result.cancel) {
            return reject(result)
          }
          if (result.no) {
            return reject(result)
          }
          if (result.submit) {
            return resolve();
          }
        });
      }
    )
  }

  // Event Handlers
  editAwaitingPackage(awaitingPackage: any) {
    this.openPackageDeclarationFormDialog(Object.assign({}, awaitingPackage))
      .then(
        (success) => {
          console.dir(success);
        }
      )
      .catch((error) => {
        console.dir(error);
      })
  }

}