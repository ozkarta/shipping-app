import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-viewable-list-component',
  templateUrl: './package-viewable-list.component.html',
  styleUrls: ['./package-viewable-list.component.css']
})

export class ViewableListComponent implements OnInit {
  @Input('awaitingPackages') awaitingPackages: any[] = [];
  @Input('printInvoice') printInvoice: boolean = false;
  @Input('title') title: string = '';
  constructor() {

  }

  ngOnInit() {

  }

  printReceipt(pckg: any) {
    console.dir(pckg);
  }

  payReceipt(pckg: any) {
    console.dir(pckg);
  }

}