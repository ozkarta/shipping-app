import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Address } from '../../../shared/models/address.model';

@Injectable()
export class ClientAddressService {
	constructor(private http: HttpClient) {
  }
  
  public getAllAddresses(): Observable<Address[]> {
    return this.http.get<Address[]>('/api/v1/client/address')
      .pipe(
        map((addresses: Address[]) => {
          return addresses;
        }),
        catchError(
          (error: Response) => {
            return throwError(error);
          })
      );
  }
}