import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
// Components
import { ClientHomeComponent } from './home/home.component';

import { ArrivedComponent } from './arrived/arrived.component';
import { DeliveredComponent } from './delivered/delivered.component';
import { InWareHouseComponent } from './in-warehouse/in-warehouse.component';
import { OnTheWareHouseWayComponent } from './on-the-warehouse-way/on-the-warehouse-way.component';
import { SentComponent } from './sent/sent.component';

// Guard
import { ClientGuardService } from '../shared/guards/client.guard';
const appRoutes: Routes = [
  // Not Authenticated
  { path: 'client', pathMatch: 'full', redirectTo: '/client/dashboard'},
  { path: 'client/dashboard', component: ClientHomeComponent, canActivate: [ClientGuardService]},
  { path: 'client/arrived', component: ArrivedComponent, canActivate: [ClientGuardService]},
  { path: 'client/delivered', component: DeliveredComponent, canActivate: [ClientGuardService]},
  { path: 'client/in-warehouse', component: InWareHouseComponent, canActivate: [ClientGuardService]},
  { path: 'client/on-the-warehouse-way', component: OnTheWareHouseWayComponent, canActivate: [ClientGuardService]},
  { path: 'client/sent', component: SentComponent, canActivate: [ClientGuardService]},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule],
})
export class ClientRoutingModule {
}
