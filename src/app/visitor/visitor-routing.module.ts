import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
// Components
import { VisitorHomeComponent } from './home/home.component';
// Guard
import { LoggedOutGuardService } from '../shared/guards/unauthorized.guard';
const appRoutes: Routes = [
  // Not Authenticated  
  { path: 'visitor', component: VisitorHomeComponent, canActivate: [LoggedOutGuardService]},
  { path: '', redirectTo: '/visitor', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule],
})
export class OwnerRoutingModule {
}
