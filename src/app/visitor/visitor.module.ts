import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Module Imports
import { OwnerRoutingModule } from './visitor-routing.module';

// Components
import { VisitorHomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    VisitorHomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
		HttpClientModule,
		
		OwnerRoutingModule
  ],
  providers: [
  ]
})
export class VisitorModule { }
