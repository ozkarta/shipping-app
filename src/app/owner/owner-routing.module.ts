import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
// Components
import { OwnerHomeComponent } from './home/home.component';
import { OwnerAddressesComponent } from './addresses/addresses.component';
import { OwnerUserManagementComponent } from './users/users.component';
// Guard
import { OwnerGuardService } from '../shared/guards/owner.guard';
const appRoutes: Routes = [
  // Not Authenticated
  { path: 'owner', component: OwnerHomeComponent, canActivate: [OwnerGuardService]},
  { path: 'owner/addresses', component: OwnerAddressesComponent, canActivate: [OwnerGuardService]},
  { path: 'owner/users', component: OwnerUserManagementComponent, canActivate: [OwnerGuardService]},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule],
})
export class OwnerRoutingModule {
}
