import { Component, OnInit } from '@angular/core';
import { UsersService } from '../shared/services/users.service';
import { UserModel } from '../../shared/models/user.model';
import { SpinnerService } from '../../shared/services/spinner.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { OwnerUserFormComponent } from '../shared/dialogs/user-form.component/user-form.component';
@Component({
  selector: 'app-owner-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class OwnerUserManagementComponent implements OnInit {
  private openDialogReference: MatDialogRef<OwnerUserFormComponent> = <any>undefined;
  public users: UserModel[] = [];
  public userTypes: any = {
    'client': 'კლიენტი',
    'admin': 'მენეჯერი',
    'owner': 'ადმინისტრატორი'
  }
  public displayedColumns: string[] = [
    'createdAt',
    '_firstName',
    '_lastName',
    'email',
    // 'phone'
    'role',
    'action'
  ];
  constructor(private userService: UsersService,  
              private spinnerService: SpinnerService,
              public dialog: MatDialog) {

  }

  ngOnInit() {
    this.loadUsers();
  }

  openAddressFormDialog(data: any): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        this.openDialogReference = this.dialog.open(OwnerUserFormComponent, {
          width: '800px',
          data: data
        });

        this.openDialogReference.afterClosed().subscribe(result => {
          if (!result) {
            return reject();
          }
          if (result.cancel) {
            return reject(result)
          }
          if (result.no) {
            return reject(result)
          }
          if (result.submit) {
            return resolve(result.address);
          }
        });
      }
    )
  }


  loadUsers() {
    this.spinnerService.next(true);
    this.userService.getUsers()
      .subscribe(
        (users: UserModel[]) => {
          this.users = users;
          this.spinnerService.next(false);
          console.dir(this.users);
        },
        (error: Error) => {
          this.spinnerService.next(false);
          console.dir(error);
        }
      )
  }

  createUser(user: UserModel) {
    this.spinnerService.next(true);
    this.userService.createUser(user)
      .subscribe(
        (user: UserModel) => {
          this.loadUsers();
        },
        (error: Error) => {
          console.dir(error);
          this.spinnerService.next(false);
        }
      )
  }

  /// Event Handlers
  addNewUserClickHandler() {
    this.openAddressFormDialog(null)
    .then(
      (user: UserModel) => {
       this.createUser(user);
      }
    )
    .catch((error) => {
      console.dir(error);
    });
  }

  deleteUserClickHandler(user: UserModel) {
    this.spinnerService.next(true);
    this.userService.deleteUser(user._id || '')
      .subscribe(
        (success: any) => {
          this.loadUsers();
        },
        (error: Error) => {
          this.spinnerService.next(false);
        }
      )
  }

  editUserClickHandler(_user: UserModel) {
    this.openAddressFormDialog(Object.assign({}, _user))
    .then(
      (user: UserModel) => {
        console.dir(user);
      }
    )
    .catch((error) => {
      console.dir(error);
    });
  }
}