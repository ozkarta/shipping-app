import { OnInit, Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { OwnerAddressFormComponent } from '../shared/dialogs/address-form.component/address-form.component';

import { Address } from '../../shared/models/address.model';
import { AddressService } from '../shared/services/address.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';

@Component({
  selector: 'app-owner-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.css']
})

export class OwnerAddressesComponent implements OnInit {
  private openDialogReference: MatDialogRef<OwnerAddressFormComponent> = <any>undefined;
  public displayedColumns: string[] = [
    'createdAt',
    // 'updatedAt',
    'title',
    'countryCode',
    'state',
    'city',
    'zip',
    // 'address1',
    'address2Prefix',
    // 'phone'
    'action'
  ];
  public addresses: Address[] = [];

  constructor(public dialog: MatDialog,
    private addressService: AddressService,
    private spinnerService: SpinnerService) {

  }

  ngOnInit() {
    this.loadAddresses();
  }

  openAddressFormDialog(data: any): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        this.openDialogReference = this.dialog.open(OwnerAddressFormComponent, {
          width: '800px',
          data: data
        });

        this.openDialogReference.afterClosed().subscribe(result => {
          if (!result) {
            return reject();
          }
          if (result.cancel) {
            return reject(result)
          }
          if (result.no) {
            return reject(result)
          }
          if (result.submit) {
            return resolve(result.address);
          }
        });
      }
    )
  }

  editAddressClickHandler(address: Address) {
    this.openAddressFormDialog(Object.assign({}, address))
      .then(
        (address: Address) => {
          console.dir(address);
          this.updateAddress(address);
        }
      )
      .catch((error) => {
        console.dir(error);
      });
  }

  addNewAddressClickHandler() {
    this.openAddressFormDialog(null)
      .then(
        (address: Address) => {
          console.dir(address);
          this.createAddress(address);
        }
      )
      .catch((error) => {
        console.dir(error);
      });
  }

  deleteAddressClickHandler(addressId: string) {
    this.spinnerService.next(true);
    this.addressService.deleteAddress(addressId)
      .subscribe(
        (success: any) => {
          this.loadAddresses();
        },
        (error: Error) => {
          console.dir(error);
          this.spinnerService.next(false);
        }
      )
  }

  //_______________________________________________________

  createAddress(address: Address) {
    this.spinnerService.next(true);
    this.addressService.addNewAddress(address)
      .subscribe(
        (address: Address) => {
          this.loadAddresses();
        },
        (error: Error) => {
          this.spinnerService.next(false);
          console.dir(error);
        }
      )
  }

  updateAddress(address: Address) {
    if (!address || !address._id) {
      return;
    }
    this.spinnerService.next(true);
    this.addressService.updateAddress(address._id || '', address)
      .subscribe(
        (address: Address) => {
          this.loadAddresses();
        },
        (error: Error) => {
          this.spinnerService.next(false);
          console.dir(error);
        }
      )
  }

  loadAddresses() {
    this.spinnerService.next(true);
    this.addressService.getAllAddresses()
      .subscribe(
        (addresses: Address[]) => {
          this.addresses = addresses;
          this.spinnerService.next(false);
        },
        (error: Error) => {
          this.spinnerService.next(false);
          console.dir(error);
        }
      );
  }

}