import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Module Imports
import { OwnerRoutingModule } from './owner-routing.module';

// Components
import { OwnerHomeComponent } from './home/home.component';
import { OwnerAddressesComponent } from './addresses/addresses.component';
import { OwnerUserManagementComponent } from './users/users.component';

// Dialogs
import { OwnerAddressFormComponent } from './shared/dialogs/address-form.component/address-form.component';
import { OwnerUserFormComponent } from './shared/dialogs/user-form.component/user-form.component';
// Services
import { AddressService } from './shared/services/address.service';
import { UsersService } from './shared/services/users.service';

// Material
import { AngularMaterialModule } from '../shared/modules/material/mat-import.module';
// Modules
import { SharedModule } from '../shared/modules/shared/shared.module';

@NgModule({
  declarations: [
    OwnerHomeComponent,
    OwnerAddressesComponent,
    OwnerUserManagementComponent,

    OwnerAddressFormComponent,
    OwnerUserFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AngularMaterialModule,
		
    OwnerRoutingModule,
    SharedModule
  ],
  exports: [
  ],
  providers: [
    AddressService,
    UsersService,
  ],
  entryComponents: [
    OwnerAddressFormComponent,
    OwnerUserFormComponent
  ]
})
export class OwnerModule { }
