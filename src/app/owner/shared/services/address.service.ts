import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Address } from '../../../shared/models/address.model';

@Injectable()
export class AddressService {

  constructor(private http: HttpClient) {
  }

  public addNewAddress(address: Address): Observable<Address> {
    return this.http.post<Address>('/api/v1/owner/address', address)
      .pipe(
        map((address: Address) => {
          return address;
        }),
        catchError(
          (error: Response) => {
            return throwError(error);
          })
      );
  }

  public updateAddress(addressId: string, address: Address): Observable<Address> {
    return this.http.put<Address>(`/api/v1/owner/address/${addressId}`, address)
      .pipe(
        map((address: Address) => {
          return address;
        }),
        catchError(
          (error: Response) => {
            return throwError(error);
          })
      );
  }

  public getAllAddresses(): Observable<Address[]> {
    return this.http.get<Address[]>('/api/v1/owner/address')
      .pipe(
        map((addresses: Address[]) => {
          return addresses;
        }),
        catchError(
          (error: Response) => {
            return throwError(error);
          })
      );
  }

  public deleteAddress(addressId: string): Observable<any> {
    return this.http.delete(`/api/v1/owner/address/${addressId}`)
      .pipe(
        map((success: any) => {
          return success;
        }),
        catchError(
          (error: Response) => {
            return throwError(error);
          })
      );
  }
}