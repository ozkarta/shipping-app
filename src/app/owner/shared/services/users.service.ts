import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
// import { Address } from '../models/address.model';
import { UserModel } from '../../../shared/models/user.model';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient) {
  }

  getUsers():Observable<UserModel[]> {
    return this.http.get<UserModel[]>('/api/v1/owner/users')
    .pipe(
      map((_users: UserModel[]) => {
        _users = this.transformUsers(_users);
        return _users;
      }),
      catchError(
        (error: Response) => {
          return throwError(error);
        })
    );
  }

  createUser(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>('/api/v1/owner/users', user)
      .pipe(
        map((response: any) => {          
          return response;
        }),
        catchError(
          (error: Response) => {
            return throwError(error);
          })
      );
  }

  updateUser(userId: string, user: UserModel): Observable<UserModel> {
    return this.http.put<UserModel>(`/api/v1/owner/users/${userId}`, user)
      .pipe(
        map((_user: UserModel) => {
          _user = this.transformUsers([_user])[0];
          return _user;
        }),
        catchError(
          (error: Response) => {
            return throwError(error);
          })
      )
  }

  deleteUser(userId: string): Observable<any> {
    return this.http.delete(`/api/v1/owner/users/${userId}`)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(
          (error: Response) => {
            return throwError(error);
          })
      );
  }


  transformUsers(users: UserModel[]) {
    return users.map(
      (user: UserModel) => {
        user._firstName = `${user.firstName.geo} / ${user.firstName.en}`
        user._lastName = `${user.lastName.geo} / ${user.lastName.en}`
        return user;
      }
    )
  }

}