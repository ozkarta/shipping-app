import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserModel } from '../../../../shared/models/user.model';
@Component({
  selector: 'app-owner-user-form-component',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})

export class OwnerUserFormComponent {
  public user: UserModel = new UserModel();
  public isUpdateForm: boolean = false;
  public errorMessage: string = '';
  public userRoles: any[] = [
    {value: 'owner', viewValue: 'ადმინისტრატორი'},
    {value: 'admin', viewValue: 'მენეჯერი'},
    {value: 'client', viewValue: 'კლიენტი'}
  ]
  public cities: string[] = [
    'თბილისი', 'ქუთაისი', 'ბათუმი', 
  ];
  public genders: Gender[] = [
    {
      value: 'MALE',
      viewValue: 'მამრობითი'
    },
    {
      value: 'FEMALE',
      viewValue: 'მდედრობითი'
    },
    {
      value: 'OTHER',
      viewValue: 'სხვა'
    }
  ];
  
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<OwnerUserFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data) {
      this.isUpdateForm = true;
      this.user = Object.assign({}, data);
    }
  }

  onNoClick() {
    this.dialogRef.close({ cancel: true });
  }

  onYesClick() {
    this.dialogRef.close({ submit: true, address: this.user });
  }
}

interface Gender {
  value: string;
  viewValue: string;
}