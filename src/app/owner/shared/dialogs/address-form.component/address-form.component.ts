import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Address } from '../../../../shared/models/address.model';
@Component({
  selector: 'app-owner-address-form-component',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css']
})

export class OwnerAddressFormComponent {
  public address: Address = <Address>{};

  public countries: any[] = [
    {viewName: 'აშშ', value: 'US'},
    {viewName: 'გერმანია', value: 'DE'},
    {viewName: 'უკრაინა', value: 'UKR'},
  ];
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<OwnerAddressFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data) {
      this.address = Object.assign({}, data);
    }
  }

  onNoClick() {
    this.dialogRef.close({ cancel: true });
  }

  onYesClick() {
    this.dialogRef.close({ submit: true, address: this.address });
  }
}