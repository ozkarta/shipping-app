import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthUser } from '../shared/models/auth-user.model';
import { AuthService } from '../shared/services/auth.service';
import { SpinnerService } from '../shared/services/spinner.service';
import { UserModel } from '../shared/models/user.model';
@Component({
	templateUrl: './login.component.html',
	selector: 'app-login-component',
	styleUrls: ['login.component.css']
})

export class LoginComponent implements OnInit{
	user: AuthUser;
	constructor(private authService: AuthService,
							private spinnerService: SpinnerService,
							private router: Router) {
		this.user = new AuthUser();
	}

	ngOnInit() {
		
	}

	loginClickHandler(event: Event) {
		this.spinnerService.next(true);
		this.authService.login(this.user)
			.subscribe(
				(success: any) => {
					// this.spinnerService.next(false);
					this.authService.getMe()
					.subscribe(
						// TODO fix this
						(user: UserModel) => {
							console.dir(user);
							this.spinnerService.next(false);
							if (user) {
								this.router.navigate([user.role]);
							}
						},
						(error: Error) => {
							this.spinnerService.next(false);
						}
					);
				},
				(error: Error) => {
					console.dir(error);
					this.spinnerService.next(false);
				}
			);
		event.preventDefault();
	}
}