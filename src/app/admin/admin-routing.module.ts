import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
// Components
import { AdminHomeComponent } from './components/home/home.component';
// Guard
import { AdminGuardService } from '../shared/guards/admin.guard';
const appRoutes: Routes = [
  // Not Authenticated
  { path: 'admin', redirectTo: '/admin/home', pathMatch: 'full'},
  { path: 'admin/home', component: AdminHomeComponent, canActivate: [AdminGuardService]},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
