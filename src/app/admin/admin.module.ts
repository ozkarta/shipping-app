import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Module Imports
import { AdminRoutingModule } from './admin-routing.module';
import { AngularMaterialModule } from '../shared/modules/material/mat-import.module';

// Components
import { AdminHomeComponent } from './components/home/home.component';

// Services

@NgModule({
  declarations: [
    AdminHomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    
    AngularMaterialModule,		
		AdminRoutingModule
  ],
  providers: [
    
  ]
})
export class AdminModule { }
