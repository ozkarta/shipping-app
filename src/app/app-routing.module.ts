import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
// Components
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
// Guards
import {LoggedOutGuardService} from './shared/guards/unauthorized.guard'
const appRoutes: Routes = [
  // Not Authenticated
  { path: 'login', component: LoginComponent, canActivate: [LoggedOutGuardService]},
  { path: 'register', component: RegisterComponent, canActivate: [LoggedOutGuardService]},
  // { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
