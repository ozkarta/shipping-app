import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

// Components
import { MainNavbar } from './nav.component';
import { AdminNavbar } from './admin/nav.component';
import { ClientNavbar } from './client/nav.component';
import { VisitorNavbar } from './visitor/nav.component';
import { OwnerNavbar } from './owner/nav.component';

import { LanguageSelectorComponent } from '../../components/language-selector/language-selector.component';

// Modules
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [
		LanguageSelectorComponent,
		MainNavbar,
		AdminNavbar,
		ClientNavbar,
		VisitorNavbar,
		OwnerNavbar
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
		HttpClientModule,
		RouterModule,
		SharedModule,

    ScrollToModule.forRoot(),
	],
	exports: [
		MainNavbar,
		LanguageSelectorComponent
	],
  providers: [
  ]
})
export class NavigationModule { }
