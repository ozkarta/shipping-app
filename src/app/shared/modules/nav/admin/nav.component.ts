import { Component } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
@Component({
	selector: 'app-admin-navbar',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.css'],
})

export class AdminNavbar {
	public navigationItems: NavigationItem[] = [
		{
			title: 'მთავარი',
			route: '/client/dashboard',
			icon: 'fa fa-globe fa-lg'
		},
	]
	constructor(private authService: AuthService,
							private router: Router) {
	}

	logOutClickHandler(event?: Event) {
		this.authService.logOut()
			.subscribe(
				(result: boolean) => {
					this.router.navigate(['/']);
				}
			)
			
		// event.preventDefault();
	}

	generateId(text: string):  string {
		if (!text) {
			return '';
		}
		return text.toLocaleLowerCase().replace(/ /g, '_');
	}
}

interface NavigationItem {
	title: string;
	route: string;
	icon?: string;
	children?: NavigationItem[];
}