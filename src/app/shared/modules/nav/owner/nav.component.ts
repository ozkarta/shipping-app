import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
@Component({
	selector: 'app-owner-navbar',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.css'],
})

export class OwnerNavbar {
	public navigationItems: NavigationItem[] = [
		{
			title: 'მთავარი',
			route: '/owner',
			icon: 'fa fa-globe fa-lg'
		},
		{
			title: 'მისამართები',
			route: '/owner/addresses',
			icon: 'fa fa-globe fa-lg'
		},
		{
			title: 'მომხმარებლების მენეჯმენტი',
			route: '/owner/users',
			icon: 'fa fa-globe fa-lg'
		},
	]
	constructor(private authService: AuthService,
							private router: Router) {
	}

	logOutClickHandler(event?: Event) {
		this.authService.logOut()
			.subscribe(
				(result: boolean) => {
					this.router.navigate(['/']);
				}
			)
			
		// event.preventDefault();
	}

	generateId(text: string):  string {
		if (!text) {
			return '';
		}
		return text.toLocaleLowerCase().replace(/ /g, '_');
	}
}

interface NavigationItem {
	title: string;
	route: string;
	icon?: string;
	children?: NavigationItem[];
}