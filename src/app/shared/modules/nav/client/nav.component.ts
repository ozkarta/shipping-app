import {Component} from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
@Component({
	selector: 'app-client-navbar',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.css'],
})

export class ClientNavbar {
	public navigationItems: NavigationItem[] = [
		{
			title: 'მთავარი',
			route: '/client/dashboard',
			icon: 'fa fa-globe fa-lg'
		},
		{
			title: 'მისაღები ამანათები',
			route: '/client/on-the-warehouse-way',
			icon: 'fa fa-globe fa-lg'
		},
		{
			title: 'საწყობი',
			route: '/client/in-warehouse',
			icon: 'fa fa-globe fa-lg'
		},
		{
			title: 'გამოგზავნილი ამანათები',
			route: '/client/sent',
			icon: 'fa fa-globe fa-lg'
		},
		{
			title: 'ჩამოსული ამანათები',
			route: '/client/arrived',
			icon: 'fa fa-globe fa-lg'
		},
		{
			title: 'მიღებული ამანათები',
			route: '/client/delivered',
			icon: 'fa fa-globe fa-lg'
		},
	]
	constructor(private authService: AuthService,
							private router: Router) {
	}

	logOutClickHandler(event?: Event) {
		this.authService.logOut()
			.subscribe(
				(result: boolean) => {
					this.router.navigate(['/']);
				}
			)
			
		// event.preventDefault();
	}

	generateId(text: string):  string {
		if (!text) {
			return '';
		}
		return text.toLocaleLowerCase().replace(/ /g, '_');
	}
}

interface NavigationItem {
	title: string;
	route: string;
	icon?: string;
	children?: NavigationItem[];
}