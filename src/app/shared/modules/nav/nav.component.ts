import {Component, OnInit, OnDestroy} from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs';
import { UserModel } from '../../models/user.model';
@Component({
	selector: 'app-main-navbar',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.css'],
})

export class MainNavbar implements OnInit, OnDestroy {
	private userChangeSubscription: Subscription = new Subscription();
	public user: UserModel = <any>null;
	constructor(private authService: AuthService) {}

	ngOnInit() {
		this.userChangeSubscription = this.authService.getUser
			.subscribe(
				(user: UserModel) => {
					console.dir(user);
					this.user = user;
				}
			);
	}

	ngOnDestroy() {
		if (this.userChangeSubscription) {
			this.userChangeSubscription.unsubscribe();
		}
	}
}