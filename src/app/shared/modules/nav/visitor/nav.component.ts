import { Component, HostListener, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
	selector: 'app-visitor-navbar',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.css'],
})

export class VisitorNavbar implements OnInit, OnDestroy{
	public pageYOffset: number = 0;
	private routeUrlSubscription: Subscription = new Subscription();
	public currentUrl = '';
	

	@HostListener('window:scroll', ['$event'])
	doSomething(event: Event) {
		this.pageYOffset = window.pageYOffset;
	}

	constructor(private route: ActivatedRoute,
						  private router: Router) {

	}

	ngOnInit() {
		// this.subscribeUrlParameters();
		this.subscribeRouter();
	}

	ngOnDestroy() {
		if (this.routeUrlSubscription) {
			this.routeUrlSubscription.unsubscribe();
		}		
	}


  subscribeUrlParameters() {
    this.routeUrlSubscription = this.route.params.subscribe(params => {
      console.log(params);
    });
	}
	
	subscribeRouter() {
		this.router.events.subscribe((event: any) => {
			if (event instanceof NavigationEnd) {
				this.currentUrl = (<NavigationEnd>event).urlAfterRedirects;
			} 
		})
	}

	shouldRenderScrollTo() {
		return this.currentUrl === '/visitor';
	}
}