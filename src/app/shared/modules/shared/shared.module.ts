import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';


// Pipes
import { TranslatePipe }   from '../../pipes/translate.pipe';
// Directives 
import { GeoInputDirective } from '../../directives/geo-input.directive';
import { LatInputDirective } from '../../directives/lat-input.directive';

@NgModule({
  declarations: [
		TranslatePipe,
		GeoInputDirective,
		LatInputDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
		HttpClientModule,
		RouterModule,

    ScrollToModule.forRoot(),
	],
	exports: [
		TranslatePipe,
		GeoInputDirective,
		LatInputDirective,
	],
  providers: [
  ]
})
export class SharedModule { }
