import {BehaviorSubject, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
@Injectable()
export class DefaultLanguageService {
  private _defaultLanguage: BehaviorSubject<string> = new BehaviorSubject('ge');

  constructor() {
  }

  public defaultLanguage(): Observable<string> {
    return this._defaultLanguage.asObservable();
  }

  public setLanguage(language: string) {
    this._defaultLanguage.next(language);
  }
}
