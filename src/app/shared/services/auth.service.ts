import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError, of, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { AuthUser } from '../models/auth-user.model';
import { UserModel } from '../models/user.model';

@Injectable()
export class AuthService {
	private user = new BehaviorSubject<UserModel>(<any>null);
	public getUser = this.user.asObservable();

	constructor(private http: HttpClient) {
	}

	login(user: AuthUser): Observable<Response> {
		return this.http.post<Response>('/api/v1/shared/user/sign-in', user)
			.pipe(
				map((response: Response) => {
					// TODO exp dates will arrive from server
					this.setCookie('access_token', (<any>response)['token'], 365, 0);
					return response;
				}), catchError(
				(error: Response) => {
					return throwError(error);
				}));
	}

	register(user: UserModel): Observable<any> {
		return this.http.post<HttpResponse<any>>('/api/v1/shared/user/register', user)
			.pipe(
				map((response: HttpResponse<any>) => {
					this.setCookie('access_token', (<any>response)['token'], 365, 0);
					return {};
				}),
				catchError((error: HttpErrorResponse) => {
					throw(error.error);
				}) 
			);
	}

	logOut():Observable<any> {
		// this.userChangeObserver.next(null);
		localStorage.clear();
		this.user.next(<any>null);
		return of(true);
	}

	getMe():Observable<UserModel> {
		return this.http.get<UserModel>('/api/v1/shared/user/me')
		.pipe(
			map((user: UserModel) => {
				this.user.next(user);
				return user;
			}), catchError(
			(error: Response) => {
				this.user.next(<any>null);
				return throwError(error);
			}));
	}

	updateUserInfo(user: UserModel): Observable<UserModel> {
		return this.http.put<UserModel>('/api/v1/shared/user', user)
			.pipe(
				map((updatedUser: UserModel) => {
					this.user.next(updatedUser);
					return updatedUser;
				}),
				catchError((error: HttpErrorResponse) => {
					throw(error.error);
				}) 
			);
	}

	//________________________________________________________________________________________

	setCookie(cName: string = '', value: string = '', exDays: number, exSeconds: number) {
    const TmpExDays = isNaN(exDays) ? 0 : Number(exDays);
    const TmpExSeconds = isNaN(exSeconds) ? 0 : Number(exSeconds);
    const exdate = new Date();
    exdate.setDate(exdate.getDate() + TmpExDays);
    exdate.setSeconds(exdate.getSeconds() + TmpExSeconds);
    // let cValue = encodeURI(value);
    // if (TmpExDays || TmpExSeconds) {
    //   cValue += '; expires=' + exdate.toUTCString();
    // }
    localStorage.setItem(cName, value);
	}
	
	isAuthorized():Observable<boolean> {
    if (this.getCookie('access_token') !== null) {
      return of(true);
    }
    if (this.getCookie('access_token') === null && this.getCookie('refresh_token') === null) {
      return of(false);
		}
		
		return of(false);
	}
	
	getCookie(cName: string): any {
    if (localStorage.getItem(cName)) {
      return decodeURI(localStorage.getItem(cName) || '');
    }
    return '';
  }
}