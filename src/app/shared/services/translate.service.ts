import { Injectable, Inject } from '@angular/core';
import { TRANSLATIONS } from '../constants/translation'; // import our opaque token
import { DefaultLanguageService } from './default-language.service';

@Injectable()
export class TranslateService {
  private _currentLang: string = '';

  public get currentLang() {
    return this._currentLang;
  }

  // inject our translations
  constructor(@Inject(TRANSLATIONS) private _translations: any,
              private defaultLanguageService: DefaultLanguageService) {
    this.defaultLanguageService.defaultLanguage().subscribe(
      (lang: string) => {
        this.use(lang);
      }
    )
  }

  private use(lang: string): void {
    // set current language
    this._currentLang = lang;
  }

  private translate(key: string): string {
    // private perform translation
    let translation = key;

    if (this._translations[this.currentLang] && this._translations[this.currentLang][key]) {
      return this._translations[this.currentLang][key];
    }

    return translation;
  }

  public instant(key: string) {
    // call translation
    return this.translate(key);
  }
}