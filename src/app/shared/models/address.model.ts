export interface Address {
  _id?: string;
  title: string;
  countryCode: string;
  state: string;
  city: string;
  zip: string;
  address1: string;
  address2Prefix: string;
  phone: string;
  
  updatedAt?: Date;
  createdAt?: Date;
}