import { LocalizationString } from './localization-string.model';

export class UserModel {
  public firstName: LocalizationString = new LocalizationString();
  public lastName: LocalizationString = new LocalizationString();
  public gender: string = '';
  public birthDate: Date = new Date();
  public isOrganization: boolean = false;
  public organizationName: string = '';
  public identificationCode: string = '';
  public personalId: string = '';
  public email: string = '';
  public city: string = '';
  public address1: string = '';
  public address2: string = '';
  public password: string = '';
  
  public _firstName?: string;
  public _lastName?: string;
  public _id?: string;
  public uniqueUserId?: number;
	//________
	public role?: string;

  constructor() {

  }
}