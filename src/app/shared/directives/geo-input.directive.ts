import { Directive, HostListener, ElementRef, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[GeoInputDirective]'
})
export class GeoInputDirective {
  public obj: any = {
    '65': {
      originalChar: 'a',
      geo: {
        shift: 'ა',
        noShift: 'ა'
      }
    },
    '66': {
      originalChar: 'b',
      geo: {
        shift: 'ბ',
        noShift: 'ბ'
      }
    },
    '67': {
      originalChar: 'c',
      geo: {
        shift: 'ჩ',
        noShift: 'ც'
      }
    },
    '68': {
      originalChar: 'd',
      geo: {
        shift: 'დ',
        noShift: 'დ'
      }
    },
    '69': {
      originalChar: 'e',
      geo: {
        shift: 'ე',
        noShift: 'ე'
      }
    },
    '70': {
      originalChar: 'f',
      geo: {
        shift: 'ფ',
        noShift: 'ფ'
      }
    },
    '71': {
      originalChar: 'g',
      geo: {
        shift: 'გ',
        noShift: 'გ'
      }
    },
    '72': {
      originalChar: 'h',
      geo: {
        shift: 'ჰ',
        noShift: 'ჰ'
      }
    },
    '73': {
      originalChar: 'i',
      geo: {
        shift: 'ი',
        noShift: 'ი'
      }
    },
    '74': {
      originalChar: 'j',
      geo: {
        shift: 'ჟ',
        noShift: 'ჯ'
      }
    },
    '75': {
      originalChar: 'k',
      geo: {
        shift: 'კ',
        noShift: 'კ'
      }
    },
    '76': {
      originalChar: 'l',
      geo: {
        shift: 'ლ',
        noShift: 'ლ'
      }
    },
    '77': {
      originalChar: 'm',
      geo: {
        shift: 'მ',
        noShift: 'მ'
      }
    },
    '78': {
      originalChar: 'n',
      geo: {
        shift: 'ნ',
        noShift: 'ნ'
      }
    },
    '79': {
      originalChar: 'o',
      geo: {
        shift: 'ო',
        noShift: 'ო'
      }
    },
    '80': {
      originalChar: 'p',
      geo: {
        shift: 'პ',
        noShift: 'პ'
      }
    },
    '81': {
      originalChar: 'q',
      geo: {
        shift: 'ქ',
        noShift: 'ქ'
      }
    },
    '82': {
      originalChar: 'r',
      geo: {
        shift: 'ღ',
        noShift: 'რ'
      }
    },
    '83': {
      originalChar: 's',
      geo: {
        shift: 'შ',
        noShift: 'ს'
      }
    },
    '84': {
      originalChar: 't',
      geo: {
        shift: 'თ',
        noShift: 'ტ'
      }
    },
    '85': {
      originalChar: 'u',
      geo: {
        shift: 'უ',
        noShift: 'უ'
      }
    },
    '86': {
      originalChar: 'v',
      geo: {
        shift: 'ვ',
        noShift: 'ვ'
      }
    },
    '87': {
      originalChar: 'w',
      geo: {
        shift: 'ჭ',
        noShift: 'წ'
      }
    },
    '88': {
      originalChar: 'x',
      geo: {
        shift: 'ხ',
        noShift: 'ხ'
      }
    },
    '89': {
      originalChar: 'y',
      geo: {
        shift: 'ყ',
        noShift: 'ყ'
      }
    },
    '90': {
      originalChar: 'z',
      geo: {
        shift: 'ძ',
        noShift: 'ზ'
      }
    },
  }
  @Output() ngModelChange:EventEmitter<any> = new EventEmitter()
  private el: any;
  constructor(el: ElementRef) {
    this.el = el.nativeElement
  }

  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('cut', ['$event']) blockCut(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('drop', ['$event']) blockDrop(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('keydown', ['$event'])
  onInput(event: any) {
    let result = '';
    if (!this.obj[event.keyCode] || !this.obj[event.keyCode]['geo']) {
    	return;
    }
    if (event.shiftKey) {
    	result = this.obj[event.keyCode]['geo']['shift'];
    } else {
    	result = this.obj[event.keyCode]['geo']['noShift'];
    }
    this.el.value += result;
    this.ngModelChange.emit(this.el.value);
    event.preventDefault();  
  }

  @HostListener('contextmenu', ['$event']) mouseEvent(e: MouseEvent) {
    e.stopPropagation();
    e.preventDefault();
  }
}