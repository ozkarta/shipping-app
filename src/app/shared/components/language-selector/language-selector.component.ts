import { Component, OnInit } from '@angular/core';
import { DefaultLanguageService } from '../../services/default-language.service';
@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrls: ['./language-selector.component.css']
})

export class LanguageSelectorComponent implements OnInit {
  public activatedLanguage = '';
  constructor(private defaultLanguageService: DefaultLanguageService) {

  }

  ngOnInit() {
    this.defaultLanguageService.defaultLanguage().subscribe(
      (lang: string) => {
        this.activatedLanguage = lang;
      }
    )
  }

  setLanguage(language: string) {
    this.defaultLanguageService.setLanguage(language);
  }

}