import { InjectionToken } from '@angular/core';

// import translations
import { LANG_GE_NAME, LANG_GE_TRANS } from './lang-ge.language';
import { LANG_EN_NAME, LANG_EN_TRANS } from './lang-en.language';
import { LANG_RU_NAME, LANG_RU_TRANS } from './lang-ru.language';

// translation token
export const TRANSLATIONS = new InjectionToken('translations');

// all translations
export const dictionary = {
    [LANG_GE_NAME]: LANG_GE_TRANS,
    [LANG_EN_NAME]: LANG_EN_TRANS,
    [LANG_RU_NAME]: LANG_RU_TRANS,
};

// providers
export const TRANSLATION_PROVIDERS = [
    { provide: TRANSLATIONS, useValue: dictionary },
];