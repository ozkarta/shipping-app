import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserModel } from '../shared/models/user.model';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';
import { SpinnerService } from '../shared/services/spinner.service';

@Component({
  selector: 'app-register-component',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit, OnDestroy {
  public errorMessage: string = '';
  public cities: string[] = [
    'თბილისი', 'ქუთაისი', 'ბათუმი', 
  ];
  public genders: Gender[] = [
    {
      value: 'MALE',
      viewValue: 'მამრობითი'
    },
    {
      value: 'FEMALE',
      viewValue: 'მდედრობითი'
    },
    {
      value: 'OTHER',
      viewValue: 'სხვა'
    }
  ];

  public userModel: UserModel = new UserModel();
  
  constructor(private authService: AuthService,
              private router: Router,
              private spinnerService: SpinnerService) {
    // this.userModel.firstName.en = 'ozbegi';
    // this.userModel.firstName.geo = 'ოზბეგი';
    // this.userModel.lastName.en = 'kartvelishvili';
    // this.userModel.lastName.geo = 'ქართველიშვილი';
    // this.userModel.gender = 'MALE';
    // this.userModel.isOrganization = false;
    // this.userModel.personalId = '54001054561';
    // this.userModel.email = 'ozbegi1@gmail.com';
    // this.userModel.city = 'თბილისი';
    // this.userModel.address1 = 'ტაშკენტის ქუჩა';
    // this.userModel.address2 = 'N25, ბინა 10';
    // this.userModel.password = '12qwert12';
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    
  }

  registerUser() {
    this.errorMessage = '';
    this.spinnerService.next(true);
    this.authService.register(this.userModel)
      .subscribe(
        (success: any) => {
          // this.spinnerService.next(false);
          this.authService.getMe()
            .subscribe(
              (success) => {
                this.spinnerService.next(false);
                this.router.navigate(['/']);
              },
              (error: any) => {
                console.dir(error);
                this.spinnerService.next(false);
                this.errorMessage = error['msg'];
                if (error['msg'] === 'User Exists') {
                  this.errorMessage = 'ასეთი მომხმარებელი უკვე არსებობს';
                }
              }
            )
        },
        (error: any) => {
          this.spinnerService.next(false);
          console.dir(error);
          this.errorMessage = error['msg'];
          if (error['msg'] === 'User Exists') {
            this.errorMessage = 'ასეთი მომხმარებელი უკვე არსებობს';
          }
        }
      )
  }
  

}

interface Gender {
  value: string;
  viewValue: string;
}