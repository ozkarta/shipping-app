import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { Injector, APP_INITIALIZER } from '@angular/core';
import { UserModel } from './shared/models/user.model';

import { AppComponent } from './app.component';

// Router
import { AppRoutingModule } from './app-routing.module';

// Module Imports
import { NavigationModule } from './shared/modules/nav/nav.module';
import { AngularMaterialModule } from './shared/modules/material/mat-import.module';
import { DialogsModule } from './shared/modules/dialogs/dialogs.module';

import { VisitorModule } from './visitor/visitor.module';
import { AdminModule } from './admin/admin.module';
import { ClientModule } from './client/client.module';
import { OwnerModule } from './owner/owner.module';
import { SharedModule } from './shared/modules/shared/shared.module';

// Components
import { LoginComponent } from './login/login.component';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { RegisterComponent } from './register/register.component';

// Constants
import { TRANSLATION_PROVIDERS } from './shared/constants/translation';

// Services
import { SpinnerService } from './shared/services/spinner.service';
import { AuthService } from './shared/services/auth.service';
import { DefaultLanguageService } from './shared/services/default-language.service';
import { TranslateService } from './shared/services/translate.service';

// Interceptors
import { AddHeaderInterceptor } from './shared/interceptors/add-header.interceptor';
// Guards
import { LoggedOutGuardService } from './shared/guards/unauthorized.guard';
import { AdminGuardService } from './shared/guards/admin.guard';
import { ClientGuardService } from './shared/guards/client.guard';
import { OwnerGuardService } from './shared/guards/owner.guard';

export function initializeApp(authService: AuthService, injector: Injector) {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      authService.getMe()
        .subscribe(
          (user: UserModel) => {
            return resolve(true);
          },
          (error: Error) => {
            return resolve(false);
          }
        )
    });
  }
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    SpinnerComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    //_________
    SharedModule,
    
    NavigationModule,
    AngularMaterialModule,
    DialogsModule,
    VisitorModule,
    AdminModule,
    ClientModule,
    OwnerModule,


    AppRoutingModule,
  ],
  providers: [
    SpinnerService,
    AuthService,
    DefaultLanguageService,
    // Translations
    TRANSLATION_PROVIDERS, 
    TranslateService,
    // Guards
    LoggedOutGuardService,
    AdminGuardService,
    ClientGuardService,
    OwnerGuardService,
    // Interceptors
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddHeaderInterceptor,
      multi: true,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AuthService, Injector],
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
