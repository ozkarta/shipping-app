let mongoose = require('mongoose');
let autoIncrementPlugin = require('../shared/mongoose-plugins/auto-increment.plugin');
let userSchema = new mongoose.Schema({
  // SYSTEM FIELDS
  passwordHash: {type: String},
  role: {type: String, enum: ['owner', 'admin', 'client']},
  // USER FIELDS
  firstName: LocalizationString = {
    en: {type: String, trim: true},
    geo: {type: String, trim: true},
  },
  lastName: LocalizationString = {
    en: {type: String, trim: true},
    geo: {type: String, trim: true},
  },
  gender: {type: String, enum: ['MALE', 'FEMALE', 'OTHER']},
  birthDate: {type: mongoose.Schema.Types.Date},
  isOrganization: {type: mongoose.Schema.Types.Boolean},
  organizationName: {type: String, trim: true},
  identificationCode: {type: String, trim: true},
  personalId: {type: String, trim: true},
  email: {type: String, trim: true},
  city: {type: String, trim: true},
  address1: {type: String, trim: true},
  address2: {type: String, trim: true},
}, {
  timestamps: true
});

userSchema.plugin(autoIncrementPlugin, {schemaName: 'User', fieldName: 'uniqueUserId'});

let userModel = mongoose.model('User', userSchema);

exports.model = userModel;
exports.schema = userSchema;