let mongoose = require('mongoose');

let addressSchema = new mongoose.Schema({
  title: {type: String, trim: true},
  countryCode: {type: String, trim: true},
  state: {type: String, trim: true},
  city: {type: String, trim: true},
  zip: {type: String, trim: true},
  address1: {type: String, trim: true},
  address2Prefix: {type: String, trim: true},
  phone: {type: String, trim: true},
}, {
  timestamps: true
});

let addressModel = mongoose.model('Address', addressSchema);

exports.model = addressModel;
exports.schema = addressSchema;