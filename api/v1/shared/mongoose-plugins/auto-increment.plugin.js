let mongoose = require('mongoose');

let autoIncrementSchema = new mongoose.Schema({
  schemaName: {type: String},
  index: {type: Number},
}, {
  timestamps: true
});

let AutoIncrementModel = mongoose.model('indexer', autoIncrementSchema);

let defaultSchemaName = 'default';
let defaultFieldName = '_index';
let defaultStep = 1;

module.exports = exports = function autoIncrementPlugin (schema, options) {
  let schemaName = defaultSchemaName;
  let fieldName = defaultFieldName;
  let step = defaultStep; 

  if (options) {
    if (options.schemaName) {
      schemaName = options.schemaName;
    }
  
    if (options.fieldName) {
      fieldName = options.fieldName;
    }
  
    if (options.step) {
      step = options.step;
    }
  }

  let obj = {};
  obj[fieldName] = 0;
  schema.add(obj);

  schema.pre('save', async function (next) {
    try {
      let autoIncrement = await AutoIncrementModel.findOne({schemaName: schemaName}).exec();
      if (!autoIncrement) {
        autoIncrement = new AutoIncrementModel({schemaName: schemaName, index: 1});
        await autoIncrement.save();
      } else {
        autoIncrement.index = autoIncrement.index + step;
        await AutoIncrementModel.findByIdAndUpdate(autoIncrement._id, autoIncrement).exec();
      }

      this[fieldName] = autoIncrement.index;
      
      next();
    } catch (error) {
      throw(error);
    }    
  });

  schema.pre('update', function (next) {
    console.log('update');
    next();
  });

}