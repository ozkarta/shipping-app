module.exports = function (express) {
  var router = express.Router();
  //__________________________________
  
  // shared controllers
  let userController = require('../controller/shared/user.controller.js')(express);
  // Owner Controllers
  let ownerAddressController = require('../controller/owner/address.controller')(express);
  let ownerUsersController = require('../controller/owner/userss.controller')(express);
  // Client Controllers
  let clientAddressController = require('../controller/client/address.controller')(express);


  //_____________SHARED_____________________
  router.use('/shared/user', userController);

  // Owner
  router.use('/owner/address', ownerAddressController);
  router.use('/owner/users', ownerUsersController);

  // Client
  router.use('/client/address', clientAddressController);

  return router;
};
