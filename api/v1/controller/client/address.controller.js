module.exports = function (express) {
  let router = express.Router();
  let AddressModel = require('../../model/address.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');

  router.get('/', async (req, res) => {
    try {
      let addresses = await AddressModel.find({}).lean().exec();
      return res.status(200).json(addresses);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.get('/:addressId', async (req, res) => {
    if (!req || !req.params || !req.params.addressId) {
      return res.status(MSG.clientError.badRequest.code).json({ msg: 'addressId not provided' });
    }
    let addressId = req.params.addressId;
    try {
      let address = await AddressModel.findById(addressId).exec();
      return res.status(200).json(address);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });


  return router;
}