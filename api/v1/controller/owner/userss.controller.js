module.exports = function (express) {
  let router = express.Router();
  let UserModel = require('../../model/user.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let bcrypt = require('bcryptjs');
  
  router.get('/', async(req, res) => {
    try {
      let users = await UserModel.find({}).lean().exec();
      return res.status(200).json(users);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });
  
  router.post('/', async(req, res) => {
    try {
      let result = await UserModel.findOne({ email: req.body.email })
        .lean()
        .exec();

      if (result) {
        console.log('Result Exists');
        return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'User Exists');
      }

      let user = new UserModel(req.body);
      user.passwordHash = bcrypt.hashSync(req.body.password, 8);

      try {
        await user.save();
        return res.status(200).json({ auth: true });
      } catch (error) {
        console.dir(error);
        return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
      }

    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.delete('/:userId', async(req, res) => {
    if (!req || !req.params || !req.params.userId) {
      return res.status(MSG.clientError.badRequest.code).json({msg: 'userId not provided'});
    }
    let userId = req.params.userId;
    try {
      await UserModel.findByIdAndDelete(userId);
      return res.status(200).json({deleted: true});
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.put('/:userId', async(req, res) => {
   
  });

  return router;
}
