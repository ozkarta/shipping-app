module.exports = function (express) {
  let router = express.Router();
  let AddressModel = require('../../model/address.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');

  router.post('/', async (req, res) => {
    console.dir(req.body);
    let address = new AddressModel(req.body);
    try {
      let savedAddress = await address.save();
      return res.status(200).json(savedAddress);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.put('/:addressId', async (req, res) => {
    let address = new AddressModel(req.body);
    try {
      let updatedAddress = await AddressModel.findByIdAndUpdate(address.id, address).exec();
      return res.status(200).json(updatedAddress);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.delete('/:addressId', async (req, res) => {
    if (!req || !req.params || !req.params.addressId) {
      return res.status(MSG.clientError.badRequest.code).json({ msg: 'addressId not provided' });
    }
    let addressId = req.params.addressId;
    try {
      AddressModel.findByIdAndDelete(addressId).exec();
      return res.status(200).json({ deleted: true });
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.get('/', async (req, res) => {
    try {
      let addresses = await AddressModel.find({}).lean().exec();
      return res.status(200).json(addresses);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.get('/:addressId', async (req, res) => {
    if (!req || !req.params || !req.params.addressId) {
      return res.status(MSG.clientError.badRequest.code).json({ msg: 'addressId not provided' });
    }
    let addressId = req.params.addressId;
    try {
      let address = await AddressModel.findById(addressId).exec();
      return res.status(200).json(address);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });


  return router;
}